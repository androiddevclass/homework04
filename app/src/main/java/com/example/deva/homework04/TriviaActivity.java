package com.example.deva.homework04;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.deva.homework04.com.example.deva.asyncTasks.ImageDownloader;
import com.example.deva.homework04.com.example.deva.dao.Question;

import java.util.ArrayList;

public class TriviaActivity extends AppCompatActivity implements ImageDownloader.ImageSetter{

    ArrayList<Question> questions;
    int currentQuestion =0;
    String selectedvalue = null;
    int correctAnswers = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trivia);

        correctAnswers = 0;

        Intent intent = getIntent();
        if(intent.getExtras() != null){
            questions = intent.getParcelableArrayListExtra(MainActivity.QUESTION_KEY);
        }

        if(questions.size() > 0){
            showQuestion(0);
        }

        findViewById(R.id.button3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedvalue != null && selectedvalue.equals(questions.get(currentQuestion).getChoices().get(questions.get(currentQuestion).getAnswerIndex()))){
                    correctAnswers++;
                }
                showQuestion(++currentQuestion);
            }
        });

    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            selectedvalue = ((TextView)v).getText().toString();
        }
    };

    private void showQuestion(int qIndex){
        selectedvalue = null;
        currentQuestion = qIndex;
        ((LinearLayout)findViewById(R.id.linearLayoutForOptions)).removeAllViews();
        findViewById(R.id.imageView2).setVisibility(View.INVISIBLE);
        Question question = questions.get(qIndex);
        ((TextView) findViewById(R.id.q1textView)).setText("Q"+currentQuestion);
        ((TextView) findViewById(R.id.questionTextview)).setText(question.getText());
        int i=1;
        for (String choice:question.getChoices()) {
            TextView textView = new TextView(this);
            textView.setText(choice);
            textView.setOnClickListener(onClickListener);
            ((LinearLayout)findViewById(R.id.linearLayoutForOptions)).addView(textView);
        }
        findViewById(R.id.imageView2).setVisibility(View.INVISIBLE);
        ((ImageView)findViewById(R.id.imageView2)).setImageResource(0);
        if(question.getImageUrl() != null){
            new ImageDownloader(this).execute(question.getImageUrl());
        }else {
            findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void setImage(Bitmap image) {
        findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);
        ((ImageView)findViewById(R.id.imageView2)).setImageBitmap(image);
        findViewById(R.id.imageView2).setVisibility(View.VISIBLE);
    }

    @Override
    public void setImageDownloadProgress() {

    }
}
