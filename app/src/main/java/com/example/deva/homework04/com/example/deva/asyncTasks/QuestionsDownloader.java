package com.example.deva.homework04.com.example.deva.asyncTasks;

import android.os.AsyncTask;

import com.example.deva.homework04.com.example.deva.dao.Question;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by deva on 9/24/16.
 */
public class QuestionsDownloader extends AsyncTask<String, Void, ArrayList<Question>> {

    QuestionGetter requester;
    public QuestionsDownloader(QuestionGetter getter){
        this.requester = getter;
    }

    public interface QuestionGetter{
        void setQuestions(ArrayList<Question> questions);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(ArrayList<Question> questions) {
        super.onPostExecute(questions);
        requester.setQuestions(questions);
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected ArrayList<Question> doInBackground(String... params) {
        publishProgress();
        ArrayList<Question> questions = new ArrayList<>();
        try {
            URL url = new URL(params[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String json;
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            inputStream.close();
            json = sb.toString();
            JSONObject jObj = new JSONObject(json);

            JSONArray jsonArray = jObj.getJSONArray("questions");
            if (jsonArray != null) {
                int len = jsonArray.length();
                for (int i = 0; i < len; i++) {
                    Question q = new Question((JSONObject) jsonArray.get(i));
                    questions.add(q);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return questions;
    }
}
