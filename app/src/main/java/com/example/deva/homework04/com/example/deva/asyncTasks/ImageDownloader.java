package com.example.deva.homework04.com.example.deva.asyncTasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by deva on 9/24/16.
 */
public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

    ImageSetter imageSetter;

    public static interface ImageSetter{
        void setImage(Bitmap image);
        void setImageDownloadProgress();
    }

    public ImageDownloader(ImageSetter imageSetter){
        this.imageSetter=imageSetter;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        imageSetter.setImageDownloadProgress();
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        imageSetter.setImage(bitmap);
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);

    }

    @Override
    protected Bitmap doInBackground(String... params) {
        Bitmap bitmap = null;
        try {
            URL url = new URL(params[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            bitmap = BitmapFactory.decodeStream(inputStream);
            return bitmap;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
