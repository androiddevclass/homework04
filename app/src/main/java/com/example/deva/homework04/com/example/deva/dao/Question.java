package com.example.deva.homework04.com.example.deva.dao;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deva on 9/24/16.
 */
public class Question implements Parcelable {

    int id;
    String text;
    Bitmap image;
    ArrayList<String> choices;
    int answerIndex;
    String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Question(){

    }

    public Question(JSONObject jsonObject) throws JSONException {
        setId(jsonObject.getInt("id"));
        setText(jsonObject.getString("text"));
        try {
            setImageUrl(jsonObject.getString("image"));
        }catch (JSONException e){
            setImageUrl(null);
        }
        ArrayList<String> choices = new ArrayList<String>();
        JSONArray jsonArray = ((JSONObject) jsonObject.get("choices")).getJSONArray("choice");
        if (jsonArray != null) {
            int len = jsonArray.length();
            for (int i = 0; i < len; i++) {
                choices.add(jsonArray.get(i).toString());
            }
        }
        setChoices(choices);
        setAnswerIndex(((JSONObject) jsonObject.get("choices")).getInt("answer"));
    }

    protected Question(Parcel in) {
        id = in.readInt();
        text = in.readString();
        image = in.readParcelable(Bitmap.class.getClassLoader());
        choices = in.createStringArrayList();
        answerIndex = in.readInt();
        imageUrl = in.readString();
    }

    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public ArrayList<String> getChoices() {
        return choices;
    }

    public void setChoices(ArrayList<String> choices) {
        this.choices = choices;
    }

    public int getAnswerIndex() {
        return answerIndex;
    }

    public void setAnswerIndex(int answerIndex) {
        this.answerIndex = answerIndex;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(text);
        dest.writeParcelable(image, flags);
        dest.writeStringList(choices);
        dest.writeInt(answerIndex);
        dest.writeString(imageUrl);
    }
}
