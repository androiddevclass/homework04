package com.example.deva.homework04;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.example.deva.homework04.com.example.deva.asyncTasks.ImageDownloader;
import com.example.deva.homework04.com.example.deva.asyncTasks.QuestionsDownloader;
import com.example.deva.homework04.com.example.deva.dao.Question;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ImageDownloader.ImageSetter, QuestionsDownloader.QuestionGetter, View.OnClickListener {

    ArrayList<Question> questions;
//    int selectedQuestion;
    public final static String QUESTION_KEY="question";
    public static final String TRIV_URL="http://dev.theappsdr.com/apis/trivia_json/index.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.imageView).setVisibility(View.INVISIBLE);
        findViewById(R.id.button2).setEnabled(false);
        findViewById(R.id.exitbutton).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
        loadTriviaQuestions(TRIV_URL);
    }


    private void loadTriviaQuestions(String url){
        new QuestionsDownloader(this).execute(url);
    }

    @Override
    public void setImage(Bitmap image) {

    }

    @Override
    public void setImageDownloadProgress() {
        findViewById(R.id.progressBar_main).setVisibility(View.VISIBLE);
    }

    @Override
    public void setQuestions(ArrayList<Question> questions) {
        findViewById(R.id.progressBar_main).setVisibility(View.INVISIBLE);
        this.questions = questions;
        findViewById(R.id.imageView).setVisibility(View.VISIBLE);
        findViewById(R.id.button2).setEnabled(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.exitbutton:
                MainActivity.this.finish();
                break;

            case R.id.button2:
                //go to start activity
                Intent intent = new Intent(MainActivity.this, TriviaActivity.class);
                intent.putParcelableArrayListExtra(QUESTION_KEY, questions);
                startActivity(intent);

                break;
        }
    }
}
